file = '/Users/shuvayan/Desktop/log.txt'
import matplotlib.pyplot as plt
losses = []
with open(file , 'r') as f:
    for line in f.readlines():
        if line.startswith('Epoch'):
            loss =line.split(':')[1].split()[0]
            losses.append(float(loss))
    f.close()
plt.plot(losses)
plt.show()