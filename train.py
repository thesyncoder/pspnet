from model.pspnet import PSPNet
from util.util import AverageMeter, intersectionAndUnionGPU
import time
import torch
import torch.nn.functional as F

import util.transform as transform
from util.dataset import CustomData
import numpy as np
import matplotlib.pyplot as plt

model = PSPNet()
modules_ori = [model.layer0, model.layer1,
               model.layer2, model.layer3, model.layer4]
modules_new = [model.ppm, model.cls, model.aux]
model = torch.nn.DataParallel(model.cuda())
params_list = []
for module in modules_ori:
    params_list.append(dict(params=module.parameters(), lr=0.01))
for module in modules_new:
    params_list.append(dict(params=module.parameters(), lr=0.01 * 10))

optimizer = torch.optim.Adam( params_list )

value_scale=255
mean=[0.485, 0.456, 0.406]
mean=[item * value_scale for item in mean]
std=[0.229, 0.224, 0.225]
std=[item * value_scale for item in std]
ignore_label =255
#getting the data
train_transform=transform.Compose([
            transform.RandScale([0.5, 2.0]),
            # transform.RandRotate([-10, 10],
            #                      padding=mean, ignore_label=ignore_label),
            transform.RandomGaussianBlur(),
            transform.RandomHorizontalFlip(),
            transform.Crop([473, 473], crop_type='rand',
                           padding=mean, ignore_label=ignore_label),
            transform.ToTensor(),
            # transform.Normalize(mean=mean, std=std)
            ])
data= CustomData(data_root_train='/home/nibs/Program/Kalpita/train/images',
                   data_root_label='/home/nibs/Program/Kalpita/train/label',transform = train_transform )
train_loader = torch.utils.data.DataLoader(
    data, batch_size=4, shuffle=True, drop_last=True, num_workers=2 , pin_memory=True)







val_transform=transform.Compose([
            transform.Crop([473, 473], crop_type='center',
                           padding=mean, ignore_label=args.ignore_label),
            transform.ToTensor(),

            # transform.Normalize(mean=mean, std=std)
            ])
val_data=CustomData(data_root_train='/home/nibs/Program/Kalpita/test/images' , data_root_label='/home/nibs/Program/Kalpita/test/label' , transform = val_transform)
val_loader=torch.utils.data.DataLoader(
            val_data, batch_size=4, shuffle=False, num_workers=2, pin_memory=True)


def train(train_loader, model, optimizer, epoch):
    batch_time = AverageMeter()
    data_time = AverageMeter()
    main_loss_meter = AverageMeter()
    aux_loss_meter = AverageMeter()
    loss_meter = AverageMeter()
    intersection_meter = AverageMeter()
    union_meter = AverageMeter()
    target_meter = AverageMeter()

    model.train()
    end = time.time()

    max_iter = 100 * len(train_loader)

    for i, (input, target) in enumerate(train_loader):
        data_time.update(time.time() - end)

        h = int((target.size()[1] - 1) / 8 *8 + 1)
        w = int((target.size()[2] - 1) / 8 *8 + 1)
        # 'nearest' mode doesn't support align_corners mode and 'bilinear' mode is fine for downsampling
        target = F.interpolate(target.unsqueeze(1).float(), size=(
            h, w), mode='bilinear', align_corners=True).squeeze(1).long()
        input = input.cuda()
        target = target.cuda()
        target = target.float()
        target = target / 255.0
        target = target.long()
        output, main_loss, aux_loss = model(input, target)
        aux_weight = 0.4
        loss = main_loss + aux_weight * aux_loss
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        n = input.size(0)
        classes = 2
        intersection, union, target = intersectionAndUnionGPU(
            output, target, classes, 255)
        intersection, union, target = intersection.cpu(
        ).numpy(), union.cpu().numpy(), target.cpu().numpy()
        intersection_meter.update(intersection), union_meter.update(
            union), target_meter.update(target)
        accuracy = sum(intersection_meter.val) / \
            (sum(target_meter.val) + 1e-10)
        main_loss_meter.update(main_loss.item(), n)
        aux_loss_meter.update(aux_loss.item(), n)
        loss_meter.update(loss.item(), n)
        batch_time.update(time.time() - end)
        end = time.time()
        current_iter = epoch * len(train_loader) + i + 1
        iou_class = intersection_meter.sum / (union_meter.sum + 1e-10)
        accuracy_class = intersection_meter.sum / (target_meter.sum + 1e-10)
        mIoU = np.mean(iou_class)
        mAcc = np.mean(accuracy_class)
        allAcc = sum(intersection_meter.sum) / (sum(target_meter.sum) + 1e-10)
        return main_loss_meter.avg, mIoU, mAcc, allAcc


save_file = os.path.join(save_path, 'loss.csv')

with open(save_file, 'a') as f:
    f.write('loss_train,' 'mIoU_train,', 'mAcc_train,',
            'allAcc_train,', 'loss_val,', 'mIoU_val,', 'mAcc_val,', 'allAcc_val')
    f.write('\n')
f.close()

no_epochs = 500
save_freq =10
## Define the  pwd
save_path = '/home/nibs/Program/Kalpita/pspnet'
lt =[]
lv =[]


mn_loss = 10001111
lossfile = '/home/nibs/Program/Kalpita/pspnet/loss.csv'
for epoch in range(no_epochs):
    epoch_log = epoch
    loss_train, mIoU_train, mAcc_train, allAcc_train=train(
        train_loader, model, optimizer, epoch)
    lt.append(loss_train)
    

    
    loss_val, mIoU_val, mAcc_val, allAcc_val=validate(
            val_loader, model, criterion)
    lv.append(loss_val)   
    with open(save_file, 'a') as f:
        f.write(str(loss_train),',', str(mIoU_train),',', str(mAcc_train),
                str(allAcc_train),','  str(loss_val),',', str(mIoU_val),',' , str(mAcc_val),',' ,str(allAcc_val))
        f.write('\n')   
    f.close()   
    


    
    print( 'Epoch {} loss : {} meanAccuracy : {} meanIoU : {}'.format(epoch+1 ,loss_train , mAcc_train , mIoU_train))

    if (epoch_log % save_freq == 0):
        filename=os.path.join(save_path, 'models') + '/train_epoch_' + str(epoch_log) + '.pth'
        torch.save({'epoch': epoch_log, 'state_dict': model.state_dict(),
                    'optimizer': optimizer.state_dict()}, filename)
        #if epoch_log / save_freq > 2:
        #    deletename=save_path + '/train_epoch_' + \
        #    str(epoch_log - save_freq * 2) + '.pth'
        #    os.remove(deletename)
    if ( mn_loss > loss_val):
        filename = os.path.join(save_path, 'models') + \
            'best_model' + '.pth'
        torch.save({'epoch': epoch_log, 'state_dict': model.state_dict(),
                    'optimizer': optimizer.state_dict()}, filename)
        mn_loss = loss_val

    plt.plot(lt, label="training")
    plt.plot(lv, label="validation")
    plt.legend(loc="upper left")
    plt.savefig('plot.png')

    
        





def train( train_loader , model , optimizer , epoch):
    batch_time=AverageMeter()
    data_time=AverageMeter()
    main_loss_meter=AverageMeter()
    aux_loss_meter=AverageMeter()
    loss_meter=AverageMeter()
    intersection_meter=AverageMeter()
    union_meter=AverageMeter()
    target_meter=AverageMeter()

    model.train()
    end=time.time()

    max_iter=args.epochs * len(train_loader)

    for i, (input, target) in enumerate(train_loader):
        data_time.update(time.time() - end)

        h=int((target.size()[1] - 1) / 8 * 8 + 1)
        w=int((target.size()[2] - 1) / 8 * 8 + 1)
        # 'nearest' mode doesn't support align_corners mode and 'bilinear' mode is fine for downsampling
        target=F.interpolate(target.unsqueeze(1).float(), size=(
                h, w), mode='bilinear', align_corners=True).squeeze(1).long()
        input=input.cuda()
        target=target.cuda()
        model = model.cuda()
        output, main_loss, aux_loss=model(input, target)

        aux_weight = 0.4
        loss=main_loss + aux_weight * aux_loss
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        n=input.size(0)
        classes = 2
        intersection, union, target=intersectionAndUnionGPU(
            output, target, classes, 255)
        intersection, union, target=intersection.cpu(
        ).numpy(), union.cpu().numpy(), target.cpu().numpy()
        intersection_meter.update(intersection), union_meter.update(
            union), target_meter.update(target)
        accuracy=sum(intersection_meter.val) / (sum(target_meter.val) + 1e-10)
        main_loss_meter.update(main_loss.item(), n)
        aux_loss_meter.update(aux_loss.item(), n)
        loss_meter.update(loss.item(), n)
        batch_time.update(time.time() - end)
        end=time.time()
        current_iter=epoch * len(train_loader) + i + 1
        iou_class=intersection_meter.sum / (union_meter.sum + 1e-10)
        accuracy_class=intersection_meter.sum / (target_meter.sum + 1e-10)
        mIoU=np.mean(iou_class)
        mAcc=np.mean(accuracy_class)
        allAcc=sum(intersection_meter.sum) / (sum(target_meter.sum) + 1e-10)
        return main_loss_meter.avg, mIoU, mAcc, allAcc

def validate(val_loader, model, criterion):

    batch_time=AverageMeter()
    data_time=AverageMeter()
    loss_meter=AverageMeter()
    intersection_meter=AverageMeter()
    union_meter=AverageMeter()
    target_meter=AverageMeter()

    model.eval()
    end=time.time()

    for i, (input, target) in enumerate(val_loader):
        data_time.update(time.time() - end)
        input=input.cuda()
        target=target.cuda()
        output=model(input)
        output=F.interpolate(output, size=target.size()[
                                 1:], mode='bilinear', align_corners=True)
        loss=criterion(output, target)
        n=input.size(0)
        loss=torch.mean(loss)

        output=output.max(1)[1]
        intersection, union, target=intersectionAndUnionGPU(
            output, target, args.classes, args.ignore_label)

        intersection, union, target=intersection.cpu(
        ).numpy(), union.cpu().numpy(), target.cpu().numpy()
        intersection_meter.update(intersection), union_meter.update(
            union), target_meter.update(target)

        accuracy=sum(intersection_meter.val) / (sum(target_meter.val) + 1e-10)
        loss_meter.update(loss.item(), input.size(0))
        batch_time.update(time.time() - end)
        end=time.time()

        iou_class=intersection_meter.sum / (union_meter.sum + 1e-10)
        accuracy_class=intersection_meter.sum / (target_meter.sum + 1e-10)
        mIoU=np.mean(iou_class)
        mAcc=np.mean(accuracy_class)
        allAcc=sum(intersection_meter.sum) / (sum(target_meter.sum) + 1e-10)


        return loss_meter.avg, mIoU, mAcc, allAcc








