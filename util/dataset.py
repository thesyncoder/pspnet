import os
import numpy as np
import cv2
from torch.utils.data import Dataset
from torchvision import transforms
from PIL import Image
import numpy as np



def make_data(label  , train):
    labelImages =[ os.path.join(label,path)  for path in os.listdir(label)]
    trainImages = [os.path.join(train,path) for path in os.listdir(train)]
    assert( len(labelImages ) == len(trainImages))
    data = [[trainImages[i],labelImages[i]] for i in range(len(labelImages))]
    return data
    

class CustomData(Dataset):
    def __init__(self , split ='train' , data_root_train = None , data_root_label = None , transform = None):
        self.split = split
        self.data_list = make_data( data_root_label , data_root_train)
        # transforms = [transforms.Resize((881, 1465))]
        # for t in transform:
        #     transforms.append(t)
        # self.transform = transforms.Compose(transforms)
        self.transform = transform


    def __len__(self):
        return len(self.data_list)

    def __getitem__(self , index):
        image_path , label_path =self.data_list[index]
        # BGR 3 channel ndarray wiht shape H * W * 3
        image = cv2.imread(image_path, cv2.IMREAD_COLOR)
        # convert cv2 read image from BGR order to RGB order
        # image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        image = np.float32(image)
        # GRAY 1 channel ndarray with shape H * W
        label = cv2.imread(label_path, cv2.IMREAD_GRAYSCALE)
        

        if image.shape[0] != label.shape[0] or image.shape[1] != label.shape[1]:
            raise (RuntimeError("Image & label shape mismatch: " +
                                image_path + " " + label_path + "\n"))
        if self.transform is not None:
            # image = self.transform(Image.fromarray(
            #     image.astype('uint8'), 'RGB'))
            # label = self.transform(Image.fromarray(
            #     label.astype('uint8')))
            image , label = self.transform( image , label)
            
        
        return image, label


# dataa = CustomData(data_root_train='/Users/shuvayan/Downloads/train/images',
#                    data_root_label='/Users/shuvayan/Downloads/train/label')
    
# 100 , h = 881 , w = 1465
# img  ,label = dataa[0]

# print( np.unique(label.numpy()))
# label = label.float32()
# label = label / 255.0
# label = label.long()
# print(np.unique(label.numpy()))

# h =[]
# w =[]
# for img , label in dataa:
#     h.append( img.shape[1])
#     w.append( img.shape[2])
#     assert( img.shape[1] == label.shape[1])
#     assert( label.shape[2] ==  img.shape[2])

# print(len(h))
# print( np.mean(np.unique(h)))
# print(np.mean(np.unique(w)))



