## Pytorch PSPNET

#### File structure :

* Dataset : Contains the segmentation details of the dataset , the colour values and the
different attributes.

* Model : Contains the implementation of different models used in the project.

* Utils : Contains dataset.py which creates the dataset


